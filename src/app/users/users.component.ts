import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users tr { cursor: default; }
    .users tr:hover { background: #ecf0f1; } 
  `]
})
export class UsersComponent implements OnInit {

  users = [
    {name:'John',email:'john@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
  ]
  

  constructor() { }

  ngOnInit() {
  }

}
